<?php

$books = [["id" => 1, "title" => "吾輩は猫である", "author" => "夏目漱石", "yearPublished" => "1911"],
["id" => 2, "title" => "こころ", "author" => "夏目漱石", "yearPublished" => "1927"],
["id" => 3, "title" => "坊ちゃん", "author" => "夏目漱石", "yearPublished" => "1950"]];

echo "<h2>Book Review</h2>
      <table  border='1'>
          <tr>
            <th>タイトル</th>
            <th>著者</th>
            <th>出版年</th>
          </tr>
    ";

foreach($books as $array => $book){
    echo "<tr><td>".$book["title"]."</td><td>".$book["author"]."</td><td>".$book["yearPublished"]."</td></tr>";
};

echo "</table>";

?>
