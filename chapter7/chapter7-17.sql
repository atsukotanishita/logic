SELECT
  d.name,
  e.name,
  salary
FROM
  Employee AS e
  INNER JOIN Department AS d ON d.dep_id = e.dep_id
WHERE
  salary = (
    SELECT
      MAX(salary)
    FROM
      Employee AS e2
    WHERE
      e.dep_id = e2.dep_id
  );
