SELECT
  dep_id,
  AVG(salary)
FROM
  Employee
GROUP BY
  dep_id
HAVING
  AVG(salary) >= 300000;
