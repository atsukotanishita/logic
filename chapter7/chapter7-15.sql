SELECT
  Employee.name,
  Department.room
FROM
  Employee
  INNER JOIN Department ON Employee.dep_id = Department.dep_id
ORDER BY
  Department.room ASC;
