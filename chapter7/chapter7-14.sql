SELECT
  name,
  salary,
  salary - (
    SELECT
      AVG(salary)
    from
      Employee
  ) AS difference
FROM
  Employee;
