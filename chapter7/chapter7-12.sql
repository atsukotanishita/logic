INSERT INTO
  salary_summary(max_salary, min_salary, avg_salary)
SELECT
  MAX(salary),
  MIN(salary),
  AVG(salary)
FROM
  Employee;
