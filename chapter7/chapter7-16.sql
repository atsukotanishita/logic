SELECT
  gender,
  name,
  birthday
FROM
  Employee
WHERE
  birthday IN (
    SELECT
      MIN(birthday)
    FROM
      Employee
    GROUP BY
      gender
  );
