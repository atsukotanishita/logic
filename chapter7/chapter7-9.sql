SELECT
  dep_id,
  GROUP_CONCAT(name SEPARATOR ',') AS name
FROM
  Employee
GROUP BY
  dep_id;
