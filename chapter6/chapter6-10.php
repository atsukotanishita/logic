<?php

$startTime = "09:30";

$endTime = "18:45";

$startTimeMin = substr($startTime, 0, 2) * 60 + substr($startTime, -2);

$endTimeMin = substr($endTime, 0, 2) * 60 + substr($endTime, -2);

$difference = $endTimeMin - $startTimeMin;

$hour = floor($difference / 60);

$min = $difference % 60;

echo sprintf('%02d', $hour) . ":" . sprintf('%02d', $min);
