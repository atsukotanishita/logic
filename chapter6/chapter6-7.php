<?php

$totalPrice = 1250;

$point = 500;

$fraction = $totalPrice % 1000;

if ($fraction <= $point) {
    echo number_format($totalPrice - $fraction) . "円\n";
    echo ($point - $fraction) . "ポイント";
} else {
    echo $totalPrice . "円\n";
    echo $point . "ポイント";
}
