<?php

$time = 900;

$min = substr($time, -2);

$hour = rtrim($time, $min);

echo sprintf('%02d', $hour) . ":" . $min;
