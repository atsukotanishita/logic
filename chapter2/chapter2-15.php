<?php

for ($i = 1; $i <= 100; $i++) {
    if ($i % 3 === 0 && $i % 5 === 0) {
        echo "サンッゴフー！\n";
    } elseif ($i % 3 === 0) {
        echo "サンッ！\n";
    } elseif ($i % 5 === 0) {
        echo "ゴフ。\n";
    } else {
        echo $i . "\n";
    }
}
