<?php

$num1 = 0;
$num2 = 1;

$f = [];
$f[0] = $num1;
$f[1] = $num2;

for ($i = 2; $i < 15; $i++) {
    $f[$i] = $f[$i - 1] + $f[$i - 2];
}

foreach ($f as $k => $v) {
    echo $v . "\n";
}
