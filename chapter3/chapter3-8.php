<?php


$users = [
    ["name" => "Tom", "age" => "20", "country" => "USA"],
    ["name" => "John", "age" => "25", "country" => "France"],
    ["name" => "Lee", "age" => "23", "country" => "China"]
];

foreach ($users as $key => $array) {
    $array["age"] += 1;
    if ($array["country"] === "USA") {
        $array["country"] = "United State of America";
    }
    $users[$key] = $array;   //この工程を書くことができなかった
}
print_r($users);
